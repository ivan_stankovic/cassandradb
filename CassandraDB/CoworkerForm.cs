﻿using CassandraDB.Helper;
using CassandraDB.UIController;
using DataModel;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class CoworkerForm : Form
    {
        private WorkersControllerUI wController;
        private Guid ID;

        public CoworkerForm()
        {
            InitializeComponent();

            wController = new WorkersControllerUI();
            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtPersonID) &&
                Validation.IsNumber(txtPersonID) &&
                Validation.IsStringEmpty(txtFirstName) &&
                Validation.IsStringEmpty(txtLastName) &&
                Validation.IsNumber(txtCompesation))
            {
                wController.addNewCoworker(txtFirstName, txtLastName, txtPersonID, txtCompesation, dateFrom, dateTo);
                cleanFields();

                refreshCoworker();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtFirstName) &&
                Validation.IsStringEmpty(txtLastName) &&
                Validation.IsNumber(txtCompesation))
            {
                wController.editCoworker(ID, txtFirstName, txtLastName, txtPersonID, txtCompesation, dateFrom, dateTo);
                cleanFields();

                refreshCoworker();
            }
        }

        private void CoworkerForm_Load(object sender, EventArgs e)
        {
            refreshCoworker();
            cleanFields();
        }

        public void refreshCoworker()
        {
            wController.showAllCoworkers(dataGridView1);
            AdjustColumnOrder();
        }

        public void cleanFields()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPersonID.Text = "";
            txtCompesation.Text = "";
            dateTo.Value = DateTime.Today;
            dateFrom.Value = DateTime.Today;
            txtPersonID.Enabled = true;
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
        }

        private void AdjustColumnOrder()
        {
            dataGridView1.Columns["PersonID"].DisplayIndex = 0;
            dataGridView1.Columns["FirstName"].DisplayIndex = 1;
            dataGridView1.Columns["LastNAme"].DisplayIndex = 2;
            dataGridView1.Columns["DateFrom"].DisplayIndex = 3;
            dataGridView1.Columns["DateTo"].DisplayIndex = 4;
            dataGridView1.Columns["Compensation"].DisplayIndex = 5;
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtPersonID.Enabled = false;
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            CoWorker cw = new CoWorker();
            cw = (DataModel.CoWorker)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = cw.CoWorkerID;
            Person person = new Person();
            person = (DataModel.Person)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            txtPersonID.Text = person.PersonID;
            txtFirstName.Text = person.FirstName;
            txtLastName.Text = person.LastName;
            txtCompesation.Text = cw.Compensation.ToString();
            dateFrom.Value = Convert.ToDateTime(cw.DateFrom);
            dateTo.Value = Convert.ToDateTime(cw.DateTo);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cleanFields();
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            CoWorker cw = new CoWorker();
            cw = (DataModel.CoWorker)dataGridView1.Rows[rowToDelete].DataBoundItem;
            wController.removeCoworker(cw);

            refreshCoworker();
        }
    }
}