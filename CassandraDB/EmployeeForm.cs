﻿using CassandraDB.Helper;
using CassandraDB.UIController;
using DataModel;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class EmployeeForm : Form
    {
        private WorkersControllerUI wController;
        private ProjectControllerUI pController;
        private Guid ID;

        public EmployeeForm()
        {
            InitializeComponent();
            wController = new WorkersControllerUI();
            pController = new ProjectControllerUI();

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);

            cbWork.DataSource = null;
            cbWork.DataSource = pController.showTeams();
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void Employee_Load(object sender, EventArgs e)
        {
            refreshEmployee();
            cleanFields();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtPersonID) &&
                Validation.IsNumber(txtPersonID) &&
                Validation.IsStringEmpty(txtFirstName) &&
                Validation.IsStringEmpty(txtLastName) &&
                Validation.IsSelectedItem(cbWork, "department"))
            {
                wController.addNewEmployee(txtFirstName, txtLastName, txtPersonID, cbWork, checkBox1);
                cleanFields();

                refreshEmployee();
            }
        }

        public void refreshEmployee()
        {
            wController.showAllEmployee(dataGridView1);
            AdjustColumnOrder();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtFirstName) &&
                  Validation.IsStringEmpty(txtLastName))
            {
                wController.editEmployee(ID, txtFirstName, txtLastName, txtPersonID, cbWork, checkBox1);
                cleanFields();

                refreshEmployee();
            }
        }

        public void cleanFields()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPersonID.Text = "";
            checkBox1.Checked = false;
            cbWork.SelectedIndex = -1;
            txtPersonID.Enabled = true;
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
        }

        private void AdjustColumnOrder()
        {
            dataGridView1.Columns["PersonID"].DisplayIndex = 0;
            dataGridView1.Columns["FirstName"].DisplayIndex = 1;
            dataGridView1.Columns["LastNAme"].DisplayIndex = 2;
            dataGridView1.Columns["WorkInDepartmant"].DisplayIndex = 3;
            dataGridView1.Columns["LeaderOfDepartmant"].DisplayIndex = 4;
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtPersonID.Enabled = false;
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Employee employee = new Employee();
            employee = (DataModel.Employee)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = employee.EmployeeID;
            Person person = new Person();
            person = (DataModel.Person)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            txtPersonID.Text = person.PersonID;
            txtFirstName.Text = person.FirstName;
            txtLastName.Text = person.LastName;
            cbWork.SelectedItem = employee.WorkInDepartmant;
            checkBox1.Checked = employee.LeaderOfDepartmant;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cleanFields();
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Employee employee = new Employee();
            employee = (DataModel.Employee)dataGridView1.Rows[rowToDelete].DataBoundItem;
            wController.removeEmployee(employee);

            refreshEmployee();
        }
    }
}