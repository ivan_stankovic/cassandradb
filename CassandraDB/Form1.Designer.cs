﻿namespace CassandraDB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCoWorkers = new System.Windows.Forms.Button();
            this.btnEmployee = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnOrganizationUnit = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPaymentBasis = new System.Windows.Forms.Button();
            this.btnPayment = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnTypeOfTasks = new System.Windows.Forms.Button();
            this.btnTasks = new System.Windows.Forms.Button();
            this.btnProject = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCoWorkers);
            this.groupBox1.Controls.Add(this.btnEmployee);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 99);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Workers";
            // 
            // btnCoWorkers
            // 
            this.btnCoWorkers.Location = new System.Drawing.Point(137, 22);
            this.btnCoWorkers.Name = "btnCoWorkers";
            this.btnCoWorkers.Size = new System.Drawing.Size(126, 61);
            this.btnCoWorkers.TabIndex = 1;
            this.btnCoWorkers.Text = "Coworker";
            this.btnCoWorkers.UseVisualStyleBackColor = true;
            this.btnCoWorkers.Click += new System.EventHandler(this.btnCoWorkers_Click);
            // 
            // btnEmployee
            // 
            this.btnEmployee.Location = new System.Drawing.Point(5, 22);
            this.btnEmployee.Name = "btnEmployee";
            this.btnEmployee.Size = new System.Drawing.Size(126, 61);
            this.btnEmployee.TabIndex = 0;
            this.btnEmployee.Text = "Employee";
            this.btnEmployee.UseVisualStyleBackColor = true;
            this.btnEmployee.Click += new System.EventHandler(this.btnEmployee_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnProject);
            this.groupBox2.Controls.Add(this.btnOrganizationUnit);
            this.groupBox2.Location = new System.Drawing.Point(298, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(274, 99);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Organization";
            // 
            // btnOrganizationUnit
            // 
            this.btnOrganizationUnit.Location = new System.Drawing.Point(10, 22);
            this.btnOrganizationUnit.Name = "btnOrganizationUnit";
            this.btnOrganizationUnit.Size = new System.Drawing.Size(126, 61);
            this.btnOrganizationUnit.TabIndex = 3;
            this.btnOrganizationUnit.Text = "Organization Unit";
            this.btnOrganizationUnit.UseVisualStyleBackColor = true;
            this.btnOrganizationUnit.Click += new System.EventHandler(this.btnOrganizationUnit_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPaymentBasis);
            this.groupBox3.Controls.Add(this.btnPayment);
            this.groupBox3.Location = new System.Drawing.Point(12, 130);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 99);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Payments";
            // 
            // btnPaymentBasis
            // 
            this.btnPaymentBasis.Location = new System.Drawing.Point(137, 23);
            this.btnPaymentBasis.Name = "btnPaymentBasis";
            this.btnPaymentBasis.Size = new System.Drawing.Size(126, 61);
            this.btnPaymentBasis.TabIndex = 2;
            this.btnPaymentBasis.Text = "Payment Basis";
            this.btnPaymentBasis.UseVisualStyleBackColor = true;
            this.btnPaymentBasis.Click += new System.EventHandler(this.btnPaymentBasis_Click);
            // 
            // btnPayment
            // 
            this.btnPayment.Location = new System.Drawing.Point(6, 23);
            this.btnPayment.Name = "btnPayment";
            this.btnPayment.Size = new System.Drawing.Size(125, 61);
            this.btnPayment.TabIndex = 1;
            this.btnPayment.Text = "Payment";
            this.btnPayment.UseVisualStyleBackColor = true;
            this.btnPayment.Click += new System.EventHandler(this.btnPayment_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnTypeOfTasks);
            this.groupBox4.Controls.Add(this.btnTasks);
            this.groupBox4.Location = new System.Drawing.Point(298, 130);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(274, 99);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tasks";
            // 
            // btnTypeOfTasks
            // 
            this.btnTypeOfTasks.Location = new System.Drawing.Point(142, 23);
            this.btnTypeOfTasks.Name = "btnTypeOfTasks";
            this.btnTypeOfTasks.Size = new System.Drawing.Size(126, 61);
            this.btnTypeOfTasks.TabIndex = 3;
            this.btnTypeOfTasks.Text = "Type Of Tasks";
            this.btnTypeOfTasks.UseVisualStyleBackColor = true;
            this.btnTypeOfTasks.Click += new System.EventHandler(this.btnTypeOfTasks_Click);
            // 
            // btnTasks
            // 
            this.btnTasks.Location = new System.Drawing.Point(10, 23);
            this.btnTasks.Name = "btnTasks";
            this.btnTasks.Size = new System.Drawing.Size(126, 61);
            this.btnTasks.TabIndex = 2;
            this.btnTasks.Text = "Tasks";
            this.btnTasks.UseVisualStyleBackColor = true;
            this.btnTasks.Click += new System.EventHandler(this.btnTasks_Click);
            // 
            // btnProject
            // 
            this.btnProject.Location = new System.Drawing.Point(142, 22);
            this.btnProject.Name = "btnProject";
            this.btnProject.Size = new System.Drawing.Size(126, 61);
            this.btnProject.TabIndex = 4;
            this.btnProject.Text = "Project";
            this.btnProject.UseVisualStyleBackColor = true;
            this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 261);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "NCMaster";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCoWorkers;
        private System.Windows.Forms.Button btnEmployee;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPaymentBasis;
        private System.Windows.Forms.Button btnPayment;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnOrganizationUnit;
        private System.Windows.Forms.Button btnTypeOfTasks;
        private System.Windows.Forms.Button btnTasks;
        private System.Windows.Forms.Button btnProject;
    }
}

