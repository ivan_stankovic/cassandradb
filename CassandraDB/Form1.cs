﻿using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            EmployeeForm ef = new EmployeeForm();
            ef.ShowDialog();
        }

        private void btnCoWorkers_Click(object sender, EventArgs e)
        {
            CoworkerForm cf = new CoworkerForm();
            cf.ShowDialog();
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            PaymentForm pf = new PaymentForm();
            pf.ShowDialog();
        }

        private void btnPaymentBasis_Click(object sender, EventArgs e)
        {
            PaymentBasisForm pb = new PaymentBasisForm();
            pb.ShowDialog();
        }

        private void btnTasks_Click(object sender, EventArgs e)
        {
            TaskForm tf = new TaskForm();
            tf.ShowDialog();
        }

        private void btnTypeOfTasks_Click(object sender, EventArgs e)
        {
            TypeOfTaskForms tot = new TypeOfTaskForms();
            tot.ShowDialog();
        }

        private void btnOrganizationUnit_Click(object sender, EventArgs e)
        {
            OrganizationUnitForm ouf = new OrganizationUnitForm();
            ouf.ShowDialog();
        }

        private void btnProject_Click(object sender, EventArgs e)
        {
            ProjectForm p = new ProjectForm();
            p.ShowDialog();
        }
    }
}