﻿using System;
using System.Windows.Forms;

namespace CassandraDB.Helper
{
    public static class Validation
    {
        public static bool IsStringEmpty(TextBox txt)
        {
            if (txt.Text == String.Empty || txt.Text == null || txt.Text == " ")
            {
                txt.Focus();
                MessageBox.Show("Field can't be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool IsNumber(TextBox txt)
        {
            string s = txt.Text.Trim();
            foreach (char c in s)
            {
                if (!Char.IsDigit(c))
                {
                    txt.Focus();
                    txt.Text = "";
                    MessageBox.Show("Enter a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return (s.Length > 0);
        }

        public static bool IsSelectedItem(ComboBox cb, string field)
        {
            if (cb.SelectedIndex != -1)
            {
                return true;
            }
            else
            {
                cb.Focus();
                MessageBox.Show("Choose a " + field + "", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}