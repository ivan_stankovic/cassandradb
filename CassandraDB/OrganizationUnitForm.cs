﻿using CassandraDB.Helper;
using CassandraDB.UIController;
using DataModel;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class OrganizationUnitForm : Form
    {
        private ProjectControllerUI pController;
        private Guid ID;
        public List<string> teams;

        public OrganizationUnitForm()
        {
            InitializeComponent();
            pController = new ProjectControllerUI();
            pController.showEmployee(cbPerson);

            teams = new List<string>();

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void OrganizationUnit_Load(object sender, System.EventArgs e)
        {
            refreshOrgUnit();
            cleanFields();
        }

        private void updateToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            teams.Clear();
            listView1.Items.Clear();
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            OrganizationalUnit ou = new OrganizationalUnit();
            ou = (DataModel.OrganizationalUnit)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = ou.OrganizationUnitID;
            txtName.Text = ou.OrganizationUnitName;
            txtPLace.Text = ou.Place;
            cbPerson.SelectedIndex = cbPerson.FindStringExact(ou.PersonID + " ");
            string[] team = ou.Team.Split(',');
            foreach (string item in team)
            {
                teams.Add(item);
            }
            if (teams.Count != 0)
            {
                for (int i = 0; i < teams.Count; i++)
                {
                    ListViewItem lvi = new ListViewItem(teams[i]);
                    listView1.BeginUpdate();
                    listView1.Items.Add(lvi);
                    listView1.EndUpdate();
                }
            }

            TeamForm tf = new TeamForm(this);
        }

        private void deleteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            cleanFields();
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            OrganizationalUnit ou = new OrganizationalUnit();
            ou = (DataModel.OrganizationalUnit)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removeOrgUnit(ou);

            refreshOrgUnit();
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsStringEmpty(txtPLace))
            {
                pController.addNewOrgUnit(txtPerson, txtPLace, txtName, cbPerson, teams);
                cleanFields();

                refreshOrgUnit();
            }
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsStringEmpty(txtPLace))
            {
                pController.editOrgUnit(ID, txtPerson, txtPLace, txtName, cbPerson, teams);
                cleanFields();

                refreshOrgUnit();
            }
        }

        public void refreshOrgUnit()
        {
            pController.showAllOrgUnit(dataGridView1);
        }

        private void cbPerson_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cbPerson.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPerson.SelectedItem as Employee).FirstName + " " + (cbPerson.SelectedItem as Employee).LastName;
            }
        }

        public void cleanFields()
        {
            txtPerson.Text = "";
            txtPLace.Text = "";
            txtName.Text = "";
            cbPerson.SelectedIndex = -1;
            listView1.Items.Clear();
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            TeamForm tf = new TeamForm(this);
            tf.ShowDialog();
        }
    }
}