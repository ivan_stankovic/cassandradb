﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class ParticipientForm : Form
    {
        private TaskControllerUI tController;
        private Dictionary<string, string> person;
        private TaskForm tf;

        public ParticipientForm(TaskForm _tf)
        {
            InitializeComponent();
            tController = new TaskControllerUI();
            tController.showEmployee(cbPersonID);
            person = new Dictionary<string, string>();

            tf = _tf;

            person = tf.person;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = person.ToList();

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            Person p = new Person();
            if (cbPersonID.SelectedIndex != -1)
            {
                p.PersonID = (cbPersonID.SelectedItem as Employee).PersonID;
                p.FirstName = (cbPersonID.SelectedItem as Employee).FirstName;
                p.LastName = (cbPersonID.SelectedItem as Employee).LastName;

                if (!person.ContainsKey(p.PersonID))
                {
                    person.Add(p.PersonID, p.FirstName + " " + p.LastName);
                }
            }

            refreshForm(person);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (var item in person)
            {
                ListViewItem lvi = new ListViewItem(item.Value);
                tf.listView1.BeginUpdate();
                tf.listView1.Items.Add(lvi);
                tf.listView1.EndUpdate();
            }

            tf.person = person;
            this.Close();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            KeyValuePair<string, string> p = (KeyValuePair<string, string>)dataGridView1.Rows[rowToDelete].DataBoundItem;
            person.Remove(p.Key);

            refreshForm(person);
        }

        private void cbPersonID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPersonID.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPersonID.SelectedItem as Employee).FirstName + " " + (cbPersonID.SelectedItem as Employee).LastName;
            }
        }

        private void refreshForm(Dictionary<string, string> persons)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = person.ToList();
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "Person";
        }
    }
}