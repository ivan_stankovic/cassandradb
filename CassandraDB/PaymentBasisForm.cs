﻿using CassandraDB.Helper;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class PaymentBasisForm : Form
    {
        private PaymentControllerUI pController;
        private ContextMenuStrip cms = new ContextMenuStrip();

        public PaymentBasisForm()
        {
            InitializeComponent();
            pController = new PaymentControllerUI();

            cms.Items.Add("Delete");
            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
            this.cms.Click += new System.EventHandler(this.DeleteRow_Click);

            refreshPaymentBasis();
        }

        private void DeleteRow_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.PaymentBasis pb = new DataModel.PaymentBasis();
            pb = (DataModel.PaymentBasis)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removePaymentBasis(pb);

            refreshPaymentBasis();
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtPaymentBasis) &&
                Validation.IsNumber(txtPaymentBasis))
            {
                pController.addNewPaymentBasis(txtPaymentBasis);
                txtPaymentBasis.Text = "";

                refreshPaymentBasis();
            }
        }

        public void refreshPaymentBasis()
        {
            pController.showAllPaymetBasis(dataGridView1);
        }
    }
}