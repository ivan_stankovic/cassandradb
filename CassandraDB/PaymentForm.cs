﻿using CassandraDB.Helper;
using DataModel;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class PaymentForm : Form
    {
        private PaymentControllerUI pController;
        private Guid ID;

        public PaymentForm()
        {
            InitializeComponent();
            pController = new PaymentControllerUI();
            pController.showEmployee(cbPerson);
            pController.showPaymentBasis(cbBasis);

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void cbPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPerson.SelectedIndex != -1)
            {
                txtPerson.Text = (cbPerson.SelectedItem as Employee).FirstName + " " + (cbPerson.SelectedItem as Employee).LastName;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtPayment) &&
                  Validation.IsNumber(txtPayment))
            {
                pController.editPayment(ID, txtPayment, txtPerson, dateTimePicker1, cbPerson, cbBasis);
                cleanFields();

                refreshPayment();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtPayment) &&
                Validation.IsNumber(txtPayment) &&
                Validation.IsSelectedItem(cbPerson, "employee") &&
                Validation.IsSelectedItem(cbBasis, "basis"))
            {
                double basis = Convert.ToDouble((cbBasis.SelectedItem as PaymentBasis).Basis);
                double payout = Convert.ToDouble(txtPayment.Text);
                if (payout >= basis)
                {
                    pController.addNewPayment(txtPayment, txtPerson, dateTimePicker1, cbPerson, cbBasis);
                    cleanFields();

                    refreshPayment();
                }
                else
                {
                    txtPayment.Focus();
                    MessageBox.Show("Payment must be greater than or equal basis", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void PaymentForm_Load(object sender, EventArgs e)
        {
            refreshPayment();
            cleanFields();
        }

        public void refreshPayment()
        {
            pController.showAllPaymet(dataGridView1);
        }

        public void cleanFields()
        {
            txtPerson.Text = "";
            txtPayment.Text = "";
            cbBasis.SelectedIndex = -1;
            cbPerson.SelectedIndex = -1;
            dateTimePicker1.Value = DateTime.Now;
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Payment p = new Payment();
            p = (DataModel.Payment)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = p.PaymentID;
            txtPayment.Text = p.Payout.ToString();
            dateTimePicker1.Value = Convert.ToDateTime(p.PaymentDate);
            cbBasis.SelectedIndex = cbBasis.FindStringExact(p.PaymentBasis.ToString());
            cbPerson.SelectedIndex = cbPerson.FindStringExact(p.PersonID + " ");
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cleanFields();
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Payment p = new Payment();
            p = (DataModel.Payment)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removePayment(p);

            refreshPayment();
        }
    }
}