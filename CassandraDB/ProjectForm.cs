﻿using CassandraDB.Helper;
using CassandraDB.UIController;
using DataModel;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class ProjectForm : Form
    {
        private ProjectControllerUI pController;
        private ContextMenuStrip cms = new ContextMenuStrip();

        public ProjectForm()
        {
            InitializeComponent();

            pController = new ProjectControllerUI();

            cms.Items.Add("Delete");
            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
            this.cms.Click += new System.EventHandler(this.DeleteRow_Click);

            refreshProject();
        }

        private void DeleteRow_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            Project p = new Project();

            p = (DataModel.Project)dataGridView1.Rows[rowToDelete].DataBoundItem;
            pController.removeProject(p);

            refreshProject();
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtProjectName))
            {
                pController.addNewProject(txtProjectName);
                txtProjectName.Text = "";

                refreshProject();
            }
        }

        public void refreshProject()
        {
            pController.showAllProject(dataGridView1);
        }
    }
}