﻿using CassandraDB.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class TaskForm : Form
    {
        private TaskControllerUI tController;
        public Dictionary<string, string> person;
        private Guid ID;

        public TaskForm()
        {
            InitializeComponent();
            tController = new TaskControllerUI();

            person = new Dictionary<string, string>();

            tController.showProject(cbProject);
            tController.showTypeOfTask(cbType);

            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
        }

       

        private void TaskForm_Load(object sender, EventArgs e)
        {
            refreshTask();
            cleanFields();
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
            listView1.Items.Clear();
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = t.TaskID;
            txtName.Text = t.TaskName;
            cbProject.SelectedIndex = cbProject.FindStringExact(t.Project);
            cbType.SelectedIndex = cbType.FindStringExact(t.TypesOfTask);
          
            taskDate.Value = Convert.ToDateTime(t.TaskDate);
            planedDate.Value = Convert.ToDateTime(t.PlannedEndDate);
            actualDate.Value = Convert.ToDateTime(t.ActualEndDate);
            List<string> people = new List<string>();
            string[] tmp = t.Participants.Split(',');
            foreach (string item in tmp)
            {
                people.Add(item);
            }
            if (people.Count != 0)
            {
                for (int i = 0; i < people.Count; i++)
                {
                    ListViewItem lvi = new ListViewItem(people[i]);
                    listView1.BeginUpdate();
                    listView1.Items.Add(lvi);
                    listView1.EndUpdate();

                }
            }
            person = t.WorkersOnTask;

            ParticipientForm pf = new ParticipientForm(this);

            refreshTask();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToDelete].DataBoundItem;
            tController.removeTask(t);

            refreshTask();
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            ParticipientForm pf = new ParticipientForm(this);
            pf.ShowDialog();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsSelectedItem(cbProject, "project") &&
                Validation.IsSelectedItem(cbType, "type of task"))
            {
                tController.addNewTask(txtName, cbProject, cbType, taskDate, planedDate, actualDate, person);
                cleanFields();

                refreshTask();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtName) &&
                Validation.IsSelectedItem(cbProject, "project") &&
                Validation.IsSelectedItem(cbType, "type of task"))
            {
                tController.editTask(ID, txtName, cbProject, cbType, taskDate, planedDate, actualDate, person);
                cleanFields();

                refreshTask();
            }
        }

        private void refreshTask()
        {
            tController.showAllTask(dataGridView1);
        }

        private void cleanFields()
        {
            txtName.Text = "";
            cbType.SelectedIndex = -1;
            cbProject.SelectedIndex = -1;
            taskDate.Value = DateTime.Now;
            planedDate.Value = DateTime.Now;
            actualDate.Value = DateTime.Now;
            listView1.Items.Clear();
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;

        }

        private void participientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Int32 rowToUpdate = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataModel.Task t = new DataModel.Task();
            t = (DataModel.Task)dataGridView1.Rows[rowToUpdate].DataBoundItem;
            ID = t.TaskID;
            txtName.Text = t.TaskName;
            cbProject.SelectedIndex = cbProject.FindStringExact(t.Project);
            cbType.SelectedIndex = cbType.FindStringExact(t.TypesOfTask);

            taskDate.Value = Convert.ToDateTime(t.TaskDate);
            planedDate.Value = Convert.ToDateTime(t.PlannedEndDate);
            actualDate.Value = Convert.ToDateTime(t.ActualEndDate);
            List<string> people = new List<string>();
            string[] tmp = t.Participants.Split(',');
            foreach (string item in tmp)
            {
                people.Add(item);
            }
            if (people.Count != 0)
            {
                for (int i = 0; i < people.Count; i++)
                {
                    ListViewItem lvi = new ListViewItem(people[i]);
                    listView1.BeginUpdate();
                    listView1.Items.Add(lvi);
                    listView1.EndUpdate();

                }
            }
            person = t.WorkersOnTask;

            TaskMemberForm tmf = new TaskMemberForm(this);
            tmf.Show();
        }

      
    }
}
