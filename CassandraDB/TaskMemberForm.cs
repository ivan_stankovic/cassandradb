﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class TaskMemberForm : Form
    {
        private Dictionary<string, string> person;
        private TaskForm tf;

        public TaskMemberForm(TaskForm _tf)
        {
            InitializeComponent();
            person = new Dictionary<string, string>();

            tf = _tf;

            person = tf.person;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = person.ToList();
            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "Person";
        }
    }
}