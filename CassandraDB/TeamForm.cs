﻿using CassandraDB.UIController;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class TeamForm : Form
    {
        private ProjectControllerUI pController;

        private CheckBox cb;
        private string name = "cb_";
        private List<string> list;
        private OrganizationUnitForm ouf;

        public TeamForm(OrganizationUnitForm _ouf)
        {
            InitializeComponent();
            pController = new ProjectControllerUI();
            ouf = _ouf;
            list = ouf.teams;
        }

        private void TeamForm_Load(object sender, EventArgs e)
        {
            int y = 10;
            List<string> teams = pController.showTeams();
            for (int n = 0; n < teams.Count; n++)
            {
                cb = new CheckBox();
                cb.Location = new Point(10, y);
                cb.Name = name + n.ToString();
                cb.Text = teams[n].ToString();

                Controls.Add(cb);
                y += 25;
            }

            foreach (Control c in this.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (cb.Text == list[i])
                        {
                            cb.CheckState = CheckState.Checked;
                        }
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            List<string> teams = new List<string>();
            foreach (Control c in this.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    if (cb.CheckState == CheckState.Checked)
                    {
                        teams.Add(cb.Text);
                    }
                }
            }

            StringBuilder sp = new StringBuilder();
            if (teams.Count != 0)
            {
                for (int i = 0; i < teams.Count; i++)
                {
                    ListViewItem lvi = new ListViewItem(teams[i]);
                    ouf.listView1.BeginUpdate();
                    ouf.listView1.Items.Add(lvi);
                    ouf.listView1.EndUpdate();
                    sp.Append(teams[i]);
                    if (i < teams.Count - 1)
                        sp.Append(",");
                }
            }
            ouf.teams = teams;
            this.Close();
        }
    }
}