﻿using CassandraDB.Helper;
using DataModel;
using System;
using System.Windows.Forms;

namespace CassandraDB
{
    public partial class TypeOfTaskForms : Form
    {
        private TaskControllerUI tController;
        private ContextMenuStrip cms = new ContextMenuStrip();

        public TypeOfTaskForms()
        {
            InitializeComponent();
            tController = new TaskControllerUI();

            cms.Items.Add("Delete");
            dataGridView1.ContextMenuStrip = cms;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MyDataGridView_MouseDown);
            this.cms.Click += new System.EventHandler(this.DeleteRow_Click);
        }

        private void DeleteRow_Click(object sender, EventArgs e)
        {
            Int32 rowToDelete = dataGridView1.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            TypeOfTasks tot = new TypeOfTasks();

            tot = (DataModel.TypeOfTasks)dataGridView1.Rows[rowToDelete].DataBoundItem;
            tController.removeTypeOfTask(tot);

            refreshTypeOfTask();
        }

        private void MyDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                dataGridView1.ClearSelection();
                dataGridView1.Rows[hti.RowIndex].Selected = true;
            }
        }

        public void refreshTypeOfTask()
        {
            tController.showAllTypesOfTasks(dataGridView1);
        }

        private void TypeOfTasks_Load(object sender, EventArgs e)
        {
            refreshTypeOfTask();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Validation.IsStringEmpty(txtTypeOfTasks))
            {
                tController.addNewTypeOfTask(txtTypeOfTasks);
                txtTypeOfTasks.Text = "";

                refreshTypeOfTask();
            }
        }
    }
}