﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CassandraDB
{
    internal class PaymentControllerUI
    {
        #region PaymentBasis

        public void addNewPaymentBasis(TextBox txtBasis)
        {
            double basis = Convert.ToDouble(txtBasis.Text);

            PaymentBasis pBasis = new PaymentBasis();
            pBasis.Basis = basis;
            DBBroker.Broker.Instance.InsertObject(pBasis);
        }

        public void removePaymentBasis(PaymentBasis basis)
        {
            DBBroker.Broker.Instance.DeleteObject(basis);
        }

        public void showAllPaymetBasis(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new PaymentBasis());
            List<PaymentBasis> list = ldo.OfType<PaymentBasis>().ToList<PaymentBasis>();

            dgv.DataSource = list;
        }

        public void showPaymentBasis(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new PaymentBasis());
            List<PaymentBasis> list = ldo.OfType<PaymentBasis>().ToList<PaymentBasis>();

            cmb.DataSource = list;
        }

        #endregion PaymentBasis

        #region Payment

        public void showAllPaymet(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Payment());
            List<Payment> list = ldo.OfType<Payment>().ToList<Payment>();

            dgv.DataSource = list;
        }

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Employee());
            List<Employee> list = ldo.OfType<Employee>().ToList<Employee>();

            cmb.DataSource = list;
        }

        public void addNewPayment(TextBox txtPayment, TextBox txtPerson, DateTimePicker date, ComboBox cbPersonID, ComboBox cbPaymentBasis)
        {
            double payout = Convert.ToDouble(txtPayment.Text);

            Payment p = new Payment();
            p.PersonID = (cbPersonID.SelectedItem as Person).PersonID;
            p.Person = txtPerson.Text;
            p.Payout = payout;
            p.PaymentDate = String.Format("{0:s}", date.Value);
            p.PaymentBasis = (cbPaymentBasis.SelectedItem as PaymentBasis).Basis;

            DBBroker.Broker.Instance.InsertObject(p);
        }

        public void editPayment(Guid ID, TextBox txtPayment, TextBox txtPerson, DateTimePicker date, ComboBox cbPersonID, ComboBox cbPaymentBasis)
        {
            double payout = Convert.ToDouble(txtPayment.Text);

            Payment p = new Payment();
            p.PaymentID = ID;
            p.PersonID = (cbPersonID.SelectedItem as Person).PersonID;
            p.Person = txtPerson.Text;
            p.Payout = payout;
            p.PaymentDate = String.Format("{0:s}", date.Value);
            p.PaymentBasis = (cbPaymentBasis.SelectedItem as PaymentBasis).Basis;

            DBBroker.Broker.Instance.UpdateObject(p);
        }

        public void removePayment(Payment payment)
        {
            DBBroker.Broker.Instance.DeleteObject(payment);
        }

        #endregion Payment
    }
}