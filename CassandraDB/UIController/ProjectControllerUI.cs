﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CassandraDB.UIController
{
    internal class ProjectControllerUI
    {
        #region Project

        public void addNewProject(TextBox txtProjectName)
        {
            string projectName = txtProjectName.Text;

            Project project = new Project();
            project.ProjectName = projectName;
            DBBroker.Broker.Instance.InsertObject(project);
        }

        public void removeProject(Project project)
        {
            DBBroker.Broker.Instance.DeleteObject(project);
        }

        public void showAllProject(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Project());
            List<Project> list = ldo.OfType<Project>().ToList<Project>();

            dgv.DataSource = list;
        }

        #endregion Project

        #region Organization Unit

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Employee());
            List<Employee> list = ldo.OfType<Employee>().ToList<Employee>();

            cmb.DataSource = list;
        }

        public void addNewOrgUnit(TextBox txtPerson, TextBox txtPLace, TextBox txtName, ComboBox cbPersonID, List<string> teams)
        {
            OrganizationalUnit ou = new OrganizationalUnit();
            if (cbPersonID.SelectedItem != null)
            {
                ou.PersonID = (cbPersonID.SelectedItem as Person).PersonID;
                ou.LeaderOfDepartmant = txtPerson.Text;
            }
            else
            {
                ou.PersonID = "";
            }
            ou.Place = txtPLace.Text;
            ou.OrganizationUnitName = txtName.Text;

            ou.Team = "[" + team(teams) + "]";

            DBBroker.Broker.Instance.InsertObject(ou);
        }

        public string team(List<string> teams)
        {
            StringBuilder sp = new StringBuilder();
            if (teams.Count != 0)
            {
                for (int i = 0; i < teams.Count; i++)
                {
                    sp.Append("'");
                    sp.Append(teams[i]);
                    sp.Append("'");
                    if (i < teams.Count - 1)
                        sp.Append(",");
                }
            }
            return sp.ToString();
        }

        public void editOrgUnit(Guid ID, TextBox txtPerson, TextBox txtPLace, TextBox txtName, ComboBox cbPersonID, List<string> teams)
        {
            OrganizationalUnit ou = new OrganizationalUnit();
            ou.OrganizationUnitID = ID;
            ou.PersonID = (cbPersonID.SelectedItem as Person).PersonID;
            ou.LeaderOfDepartmant = txtPerson.Text;
            ou.Place = txtPLace.Text;
            ou.OrganizationUnitName = txtName.Text;
            ou.Team = "[" + team(teams) + "]";

            DBBroker.Broker.Instance.UpdateObject(ou);
        }

        public void removeOrgUnit(OrganizationalUnit orgUnit)
        {
            DBBroker.Broker.Instance.DeleteObject(orgUnit);
        }

        public void showAllOrgUnit(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new OrganizationalUnit());
            List<OrganizationalUnit> list = ldo.OfType<OrganizationalUnit>().ToList<OrganizationalUnit>();

            dgv.DataSource = list;
        }

        public List<OrganizationalUnit> showOrgUnit()
        {
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new OrganizationalUnit());
            List<OrganizationalUnit> list = ldo.OfType<OrganizationalUnit>().ToList<OrganizationalUnit>();

            return list;
        }

        public List<string> showTeams()
        {
            string teamName = "";
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new OrganizationalUnit());
            List<OrganizationalUnit> list = ldo.OfType<OrganizationalUnit>().ToList<OrganizationalUnit>();
            List<string> teams = new List<string>();
            if (list.Count != 0)
            {
                foreach (OrganizationalUnit item in list)
                {
                    if (item.OrganizationUnitName != teamName)
                    {
                        teamName = item.OrganizationUnitName;
                        teams.Add(teamName);
                    }
                }
            }
            List<string> distinct = teams.Distinct().ToList();
            return distinct;
        }

        #endregion Organization Unit
    }
}