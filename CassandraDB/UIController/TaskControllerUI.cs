﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CassandraDB
{
    internal class TaskControllerUI
    {
        #region Type of tasks

        public void addNewTypeOfTask(TextBox txtType)
        {
            string type = txtType.Text;

            TypeOfTasks typeOfTask = new TypeOfTasks();
            typeOfTask.TypeName = type;
            DBBroker.Broker.Instance.InsertObject(typeOfTask);
        }

        public void removeTypeOfTask(TypeOfTasks type)
        {
            DBBroker.Broker.Instance.DeleteObject(type);
        }

        public void showAllTypesOfTasks(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new TypeOfTasks());
            List<TypeOfTasks> list = ldo.OfType<TypeOfTasks>().ToList<TypeOfTasks>();
            // List<DataModel.PaymentBasis> list = DBBroker.Broker.Instance.selectPaymentBasis();

            dgv.DataSource = list;
        }

        #endregion Type of tasks

        public void addNewTask(TextBox txtName, ComboBox cbProject, ComboBox cbType, DateTimePicker dateTask, DateTimePicker planDate, DateTimePicker actualDate, Dictionary<string, string> participient)
        {
            DataModel.Task t = new DataModel.Task();
            t.TaskName = txtName.Text;
            t.Project = (cbProject.SelectedItem as Project).ProjectName;
            t.TypesOfTask = (cbType.SelectedItem as TypeOfTasks).TypeName;
            t.TaskDate = String.Format("{0:s}", dateTask.Value);
            t.PlannedEndDate = String.Format("{0:s}", planDate.Value);
            t.ActualEndDate = String.Format("{0:s}", actualDate.Value);

            t.Participants = "{" + people(participient) + "}";
            DBBroker.Broker.Instance.InsertObject(t);
        }

        public void editTask(Guid ID, TextBox txtName, ComboBox cbProject, ComboBox cbType, DateTimePicker dateTask, DateTimePicker planDate, DateTimePicker actualDate, Dictionary<string, string> participient)
        {
            DataModel.Task t = new DataModel.Task();
            t.TaskID = ID;
            t.TaskName = txtName.Text;
            t.Project = (cbProject.SelectedItem as Project).ProjectName;
            t.TypesOfTask = (cbType.SelectedItem as TypeOfTasks).TypeName;
            t.TaskDate = String.Format("{0:s}", dateTask.Value);
            t.PlannedEndDate = String.Format("{0:s}", planDate.Value);
            t.ActualEndDate = String.Format("{0:s}", actualDate.Value);

            t.Participants = "{" + people(participient) + "}";
            DBBroker.Broker.Instance.UpdateObject(t);
        }

        private string people(Dictionary<string, string> participient)
        {
            StringBuilder sp = new StringBuilder();
            if (participient.Count != 0)
            {
                for (int i = 0; i < participient.Count; i++)
                {
                    var key = (from a in (participient.Select(b => b.Key).AsEnumerable())
                               select a).ToList();
                    var value = (from a in (participient.Select(b => b.Value).AsEnumerable())
                                 select a).ToList();
                    sp.Append("'");
                    sp.Append(key[i]);
                    sp.Append("':'");
                    sp.Append(value[i]);
                    sp.Append("'");
                    if (i < participient.Count - 1)
                        sp.Append(",");
                }
            }
            return sp.ToString();
        }

        public void removeTask(Task task)
        {
            DBBroker.Broker.Instance.DeleteObject(task);
        }

        public void showAllTask(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new DataModel.Task());
            List<DataModel.Task> list = ldo.OfType<DataModel.Task>().ToList<DataModel.Task>();

            dgv.DataSource = list;
        }

        public void showEmployee(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Employee());
            List<Employee> list = ldo.OfType<Employee>().ToList<Employee>();

            cmb.DataSource = list;
        }

        public void showProject(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Project());
            List<Project> list = ldo.OfType<Project>().ToList<Project>();

            cmb.DataSource = list;
        }

        public void showTypeOfTask(ComboBox cmb)
        {
            cmb.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new TypeOfTasks());
            List<TypeOfTasks> list = ldo.OfType<TypeOfTasks>().ToList<TypeOfTasks>();

            cmb.DataSource = list;
        }

        public void showDepartment(ComboBox cmb)
        {
            cmb.DataSource = null;

            string teamName = "";
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new OrganizationalUnit());
            List<OrganizationalUnit> list = ldo.OfType<OrganizationalUnit>().ToList<OrganizationalUnit>();
            List<string> teams = new List<string>();
            if (list.Count != 0)
            {
                foreach (OrganizationalUnit item in list)
                {
                    if (item.OrganizationUnitName != teamName)
                    {
                        teamName = item.OrganizationUnitName;
                        teams.Add(teamName);
                    }
                }
            }
            List<string> distinct = teams.Distinct().ToList();

            cmb.DataSource = distinct;
        }
    }
}