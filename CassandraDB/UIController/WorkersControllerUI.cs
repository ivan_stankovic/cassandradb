﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CassandraDB.UIController
{
    internal class WorkersControllerUI
    {
        #region Employee

        public void addNewEmployee(TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, ComboBox cmbWorker, CheckBox cbLeader)
        {
            Employee employee = new Employee();
            employee.FirstName = txtFirstName.Text;
            employee.LastName = txtLastName.Text;
            employee.PersonID = txtPersonID.Text;
            employee.WorkInDepartmant = (cmbWorker.SelectedItem).ToString();
            employee.LeaderOfDepartmant = cbLeader.Checked;

            DBBroker.Broker.Instance.InsertObject(employee);
        }

        public void removeEmployee(Employee employee)
        {
            DBBroker.Broker.Instance.DeleteObject(employee);
        }

        public void editEmployee(Guid ID, TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, ComboBox cmbWorker, CheckBox cbLeader)
        {
            Employee employee = new Employee();
            employee.EmployeeID = ID;
            employee.FirstName = txtFirstName.Text;
            employee.LastName = txtLastName.Text;
            employee.PersonID = txtPersonID.Text;
            employee.WorkInDepartmant = (cmbWorker.SelectedItem).ToString();
            employee.LeaderOfDepartmant = cbLeader.Checked;

            DBBroker.Broker.Instance.UpdateObject(employee);
        }

        public void showAllEmployee(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new Employee());
            List<Employee> list = ldo.OfType<Employee>().ToList<Employee>();

            dgv.DataSource = list;
        }

        #endregion Employee

        #region Coworker

        public void addNewCoworker(TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, TextBox txtCompesation, DateTimePicker from, DateTimePicker to)
        {
            CoWorker cw = new CoWorker();
            cw.FirstName = txtFirstName.Text;
            cw.LastName = txtLastName.Text;
            cw.PersonID = txtPersonID.Text;
            cw.Compensation = Convert.ToDouble(txtCompesation.Text);
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            var v = (String.Format("{0:s}", from.Value));
            DateTime t = Convert.ToDateTime(v);
            cw.DateFrom = String.Format("{0:s}", from.Value);
            cw.DateTo = String.Format("{0:s}", to.Value);

            DBBroker.Broker.Instance.InsertObject(cw);
        }

        public void editCoworker(Guid ID, TextBox txtFirstName, TextBox txtLastName, TextBox txtPersonID, TextBox txtCompesation, DateTimePicker from, DateTimePicker to)
        {
            CoWorker cw = new CoWorker();
            cw.CoWorkerID = ID;
            cw.FirstName = txtFirstName.Text;
            cw.LastName = txtLastName.Text;
            cw.PersonID = txtPersonID.Text;
            cw.Compensation = Convert.ToDouble(txtCompesation.Text);
            cw.DateFrom = String.Format("{0:s}", from.Value);
            cw.DateTo = String.Format("{0:s}", to.Value);

            DBBroker.Broker.Instance.UpdateObject(cw);
        }

        public void removeCoworker(CoWorker coworker)
        {
            DBBroker.Broker.Instance.DeleteObject(coworker);
        }

        public void showAllCoworkers(DataGridView dgv)
        {
            dgv.DataSource = null;
            List<DomainObject> ldo = DBBroker.Broker.Instance.SelectObject(new CoWorker());
            List<CoWorker> list = ldo.OfType<CoWorker>().ToList<CoWorker>();

            dgv.DataSource = list;
        }

        #endregion Coworker
    }
}