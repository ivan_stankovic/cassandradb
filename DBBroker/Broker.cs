﻿using Cassandra;
using DataModel;
using System.Collections.Generic;

namespace DBBroker
{
    public class Broker
    {
        private string host = "127.0.0.1";
        private int port = 9160;
        private string keySpace = "ncmaster";
        private ISession session;

        private static Broker instance;

        public static Broker Instance
        {
            get
            {
                if (instance == null)
                    instance = new Broker();
                return instance;
            }
        }

        public Broker()
        {
            Connect();
        }

        private void Connect()
        {
            Cluster cluster = Cluster.Builder()
             .AddContactPoint(host)
                 .Build();
            session = cluster.Connect(keySpace);
        }

        public RowSet SimpleQuery(string cqlQuery)
        {
            try
            {
                return session.Execute(cqlQuery);
            }
            catch (InvalidQueryException e)
            {
                throw e;
            }
        }

        public void InsertObject(DomainObject model)
        {
            string query = "INSERT INTO " + model.tableName() + " " + model.paramsForInsert() + " VALUES " + model.valueForInsert();
            SimpleQuery(query);
        }

        public void DeleteObject(DomainObject model)
        {
            string query = "DELETE FROM " + model.tableName() + " WHERE " + model.valueForQuery();
            SimpleQuery(query);
        }

        public void UpdateObject(DomainObject model)
        {
            string query = "UPDATE " + model.tableName() + " SET " + model.valueForUpdate() + " WHERE " + model.valueForQuery();
            SimpleQuery(query);
        }

        public List<DomainObject> SelectObject(DomainObject model)
        {
            string query = "SELECT * FROM " + model.tableName();
            RowSet result = SimpleQuery(query);
            return model.Fill(result);
        }

        //public List<PaymentBasis>  selectPaymentBasis()
        //{
        //    List<PaymentBasis> list = new List<PaymentBasis>();
        //    RowSet result = session.Execute("Select * from paymentbasis");
        //    foreach (Row row in result)
        //    {
        //        PaymentBasis pb = new PaymentBasis();
        //        pb.BasisID = (Guid)row[0];
        //        pb.Basis = Convert.ToDouble(row[1]);
        //        list.Add(pb);
        //    }
        //    return list.ToList<PaymentBasis>();
        //}
    }
}