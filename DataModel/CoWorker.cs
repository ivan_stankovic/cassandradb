﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class CoWorker : Person, DomainObject
    {
        private Guid _coWorkerID;

        private string _dateFrom;
        private string _dateTo;
        private double _compensation;

        public CoWorker()
        {
        }

        public CoWorker(Guid coWorkerID, string personID, string firstName, string lastName, string dateFrom, string dateTo, double compensation)
            : base(personID, firstName, lastName)
        {
            _coWorkerID = coWorkerID;
            _dateFrom = dateFrom;
            _dateTo = dateTo;
            _compensation = compensation;
        }

        [Browsable(false)]
        public Guid CoWorkerID
        {
            get { return _coWorkerID; }
            set { _coWorkerID = value; }
        }

        [DisplayName("Date from")]
        public string DateFrom
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_dateFrom);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _dateFrom = value; }
        }

        [DisplayName("Date to")]
        public string DateTo
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_dateFrom);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _dateTo = value; }
        }

        [DisplayName("Compensation")]
        public double Compensation
        {
            get { return _compensation; }
            set { _compensation = value; }
        }

        public string tableName()
        {
            return "coworker";
        }

        public Guid key()
        {
            return _coWorkerID;
        }

        public string paramsForInsert()
        {
            return "(coWorkerID, personID, firstName, lastName, dateFrom, dateTo, compensation)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + PersonID + "', '" + FirstName + "', '" + LastName + "', '" + _dateFrom + "', '" + _dateTo + "', " + _compensation + ")";
        }

        public string valueForUpdate()
        {
            return "firstName='" + FirstName + "', lastName='" + LastName + "', dateFrom='" + _dateFrom + "', dateTo='" + _dateTo + "', compensation=" + _compensation + " ";
        }

        public string valueForQuery()
        {
            return "coWorkerID=" + _coWorkerID + " AND personID='" + PersonID + "'";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public System.Collections.Generic.List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                CoWorker cw = new CoWorker();
                cw.CoWorkerID = (Guid)row[0];
                cw.PersonID = Convert.ToString(row[1]);
                cw.Compensation = Convert.ToDouble(row[2]);
                //var from = row[3];
                //var to = row[4];
                cw.DateFrom = Convert.ToString(row[3]);
                cw.DateTo = Convert.ToString(row[4]);
                cw.FirstName = Convert.ToString(row[5]);
                cw.LastName = Convert.ToString(row[6]);

                list.Add(cw);
            }

            return list;
        }
    }
}