﻿using Cassandra;
using System;
using System.Collections.Generic;

namespace DataModel
{
    public interface DomainObject
    {
        string tableName();

        Guid key();

        string paramsForInsert();

        string valueForInsert();

        string valueForUpdate();

        string valueForQuery();

        string valueForSelect(DomainObject domain);

        List<DomainObject> Fill(RowSet rows);
    }
}