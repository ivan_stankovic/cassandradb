﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class Employee : Person, DomainObject
    {
        private Guid _employeeID;
        private string _workInDepartmant;
        private bool _leaderOfDepartmant;

        public Employee()
        {
        }

        public Employee(Guid employeeID, string personID, string firstName, string lastName, string workInDepartmant, bool leaderOfDepartmant)
            : base(personID, firstName, lastName)
        {
            _employeeID = employeeID;
            _workInDepartmant = workInDepartmant;
            _leaderOfDepartmant = leaderOfDepartmant;
        }

        [Browsable(false)]
        public Guid EmployeeID
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

        [DisplayName("Work in department")]
        [Browsable(true)]
        public string WorkInDepartmant
        {
            get { return _workInDepartmant; }
            set { _workInDepartmant = value; }
        }

        [DisplayName("Leader of department")]
        [Browsable(true)]
        public bool LeaderOfDepartmant
        {
            get { return _leaderOfDepartmant; }
            set { _leaderOfDepartmant = value; }
        }

        public override string ToString()
        {
            return PersonID + " ";
        }

        public string tableName()
        {
            return "employee";
        }

        public Guid key()
        {
            return _employeeID;
        }

        public string paramsForInsert()
        {
            return "(employeeID, personID, firstName, lastName,	workInDepartmant, leaderOfDepartmant)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + PersonID + "', '" + FirstName + "', '" + LastName + "', '" + _workInDepartmant + "', " + _leaderOfDepartmant + ")";
        }

        public string valueForUpdate()
        {
            return "firstName='" + FirstName + "', lastName='" + LastName + "', workInDepartmant='" + _workInDepartmant + "', leaderOfDepartmant=" + _leaderOfDepartmant + "";
        }

        public string valueForQuery()
        {
            return "employeeID=" + _employeeID + " AND personID='" + PersonID + "'";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public System.Collections.Generic.List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                Employee e = new Employee();
                e.EmployeeID = (Guid)row[0];
                e.PersonID = Convert.ToString(row[1]);
                e.FirstName = Convert.ToString(row[2]);
                e.LastName = Convert.ToString(row[3]);
                e.LeaderOfDepartmant = Convert.ToBoolean(row[4]);
                e.WorkInDepartmant = Convert.ToString(row[5]);

                list.Add(e);
            }

            return list;
        }
    }
}