﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataModel
{
    public class OrganizationalUnit : DomainObject
    {
        private Guid _organizationUnitID;
        private string _organizationUnitName;
        private string _personID;
        private string _leaderOfDepartmant;
        private string _place;
        private string _team;

        public OrganizationalUnit()
        {
        }

        public OrganizationalUnit(Guid organizationUnitID, string organizationUnitName, string personID, string leaderOfDepartmant, string place, string team)
        {
            _organizationUnitID = organizationUnitID;
            _organizationUnitName = organizationUnitName;
            _personID = personID;
            _leaderOfDepartmant = leaderOfDepartmant;
            _place = place;
            _team = team;
        }

        [Browsable(false)]
        public Guid OrganizationUnitID
        {
            get { return _organizationUnitID; }
            set { _organizationUnitID = value; }
        }

        [DisplayName("Department name")]
        public string OrganizationUnitName
        {
            get { return _organizationUnitName; }
            set { _organizationUnitName = value; }
        }

        [DisplayName("Person ID")]
        public string PersonID
        {
            get { return _personID; }
            set { _personID = value; }
        }

        [DisplayName("Leader of department")]
        public string LeaderOfDepartmant
        {
            get { return _leaderOfDepartmant; }
            set { _leaderOfDepartmant = value; }
        }

        [DisplayName("Place")]
        public string Place
        {
            get { return _place; }
            set { _place = value; }
        }

        [DisplayName("Teams")]
        public string Team
        {
            get { return _team; }
            set { _team = value; }
        }

        public override string ToString()
        {
            return OrganizationUnitName.ToString();
        }

        public string tableName()
        {
            return "organizationunit";
        }

        public Guid key()
        {
            return _organizationUnitID;
        }

        public string paramsForInsert()
        {
            return "(organizationUnitID, organizationUnitName, personID, leaderOfDepartmant, place,	team)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + _organizationUnitName + "', '" + _personID + "', '" + _leaderOfDepartmant + "', '" + _place + "', " + _team + " )";
        }

        public string valueForUpdate()
        {
            return "organizationUnitName='" + _organizationUnitName + "', personID='" + _personID + "', leaderOfDepartmant='" + _leaderOfDepartmant + "', place='" + _place + "', team=" + _team + "";
        }

        public string valueForQuery()
        {
            return "organizationUnitID=" + _organizationUnitID + "";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            List<string> temp = new List<string>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                OrganizationalUnit ou = new OrganizationalUnit();
                ou.OrganizationUnitID = (Guid)row[0];
                ou.LeaderOfDepartmant = Convert.ToString(row[1]);
                ou.OrganizationUnitName = Convert.ToString(row[2]);
                ou.PersonID = Convert.ToString(row[3]);
                ou.Place = Convert.ToString(row[4]);
                temp = (List<string>)row[5];
                StringBuilder sp = new StringBuilder();
                if (temp != null)
                {
                    if (temp.Count != 0)
                    {
                        for (int i = 0; i < temp.Count; i++)
                        {
                            sp.Append(temp[i]);
                            if (i < temp.Count - 1)
                                sp.Append(",");
                        }
                    }
                }
                else
                {
                    ou.Team = "";
                }
                ou.Team = sp.ToString();
                list.Add(ou);
            }

            return list;
        }
    }
}