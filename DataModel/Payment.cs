﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class Payment : DomainObject
    {
        private Guid _paymentID;
        private string _paymentDate;
        private string _personID;
        private string _person;
        private double _payout;
        private double _paymentBasis;

        public Payment()
        {
        }

        public Payment(Guid paymetID, string paymentDate, string personID, string person, double payout, double paymentBasis)
        {
            _paymentID = paymetID;
            _paymentDate = paymentDate;
            _personID = personID;
            _person = person;
            _payout = payout;
            _paymentBasis = paymentBasis;
        }

        [Browsable(false)]
        public Guid PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }

        [DisplayName("Payout date")]
        public string PaymentDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_paymentDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _paymentDate = value; }
        }

        [DisplayName("Person ID")]
        public string PersonID
        {
            get { return _personID; }
            set { _personID = value; }
        }

        [DisplayName("Person")]
        public string Person
        {
            get { return _person; }
            set { _person = value; }
        }

        [DisplayName("Payout")]
        public double Payout
        {
            get { return _payout; }
            set { _payout = value; }
        }

        [DisplayName("Payment basis")]
        public double PaymentBasis
        {
            get { return _paymentBasis; }
            set { _paymentBasis = value; }
        }

        public string tableName()
        {
            return "payment";
        }

        public Guid key()
        {
            return _paymentID;
        }

        public string paramsForInsert()
        {
            return "(paymentID,	paymentDate, personID,	person,	payment, paymentbasis)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + _paymentDate + "', '" + _personID + "', '" + _person + "', " + _payout + ", " + _paymentBasis + ")";
        }

        public string valueForUpdate()
        {
            return "person='" + _person + "', paymentDate='" + _paymentDate + "', payment=" + _payout + ", paymentbasis=" + _paymentBasis + "";
        }

        public string valueForQuery()
        {
            return "paymentID=" + _paymentID + " AND personID='" + _personID + "'";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public System.Collections.Generic.List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                Payment p = new Payment();
                p.PaymentID = (Guid)row[0];
                p.PersonID = Convert.ToString(row[1]);
                p.Payout = Convert.ToDouble(row[2]);
                p.PaymentBasis = Convert.ToDouble(row[3]);
                p.PaymentDate = Convert.ToString(row[4]);
                p.Person = Convert.ToString(row[5]);

                list.Add(p);
            }

            return list;
        }
    }
}