﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class PaymentBasis : DomainObject
    {
        private Guid _basisID;
        private double _basis;

        public PaymentBasis()
        {
        }

        public PaymentBasis(Guid basisID, double basis)
        {
            _basisID = basisID;
            _basis = basis;
        }

        [Browsable(false)]
        public Guid BasisID
        {
            get { return _basisID; }
            set { _basisID = value; }
        }

        [DisplayName("Basis payment")]
        [Browsable(true)]
        public double Basis
        {
            get { return _basis; }
            set { _basis = value; }
        }

        public override string ToString()
        {
            return _basis + "";
        }

        public string tableName()
        {
            return "paymentbasis";
        }

        public Guid key()
        {
            return _basisID;
        }

        public string paramsForInsert()
        {
            return "(basisid, basis)";
        }

        public string valueForInsert()
        {
            return "(uuid(), " + _basis + ")";
        }

        public string valueForUpdate()
        {
            throw new NotImplementedException();
        }

        public string valueForQuery()
        {
            return "basisid=" + _basisID + "";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public List<DomainObject> Fill(RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                PaymentBasis pb = new PaymentBasis();
                pb.BasisID = (Guid)row[0];
                pb.Basis = Convert.ToDouble(row[1]);
                list.Add(pb);
            }

            return list;
        }
    }
}