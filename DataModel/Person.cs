﻿using System.ComponentModel;

namespace DataModel
{
    public class Person
    {
        private string _personID;
        private string _firstName;
        private string _lastName;

        public Person()
        {
        }

        public Person(string personID, string firstName, string lastName)
        {
            _personID = personID;
            _firstName = firstName;
            _lastName = lastName;
        }

        [DisplayName("ID")]
        [Browsable(true)]
        public string PersonID
        {
            get { return _personID; }
            set { _personID = value; }
        }

        [DisplayName("First name")]
        [Browsable(true)]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [DisplayName("Last name")]
        [Browsable(true)]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
    }
}