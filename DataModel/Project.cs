﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class Project : DomainObject
    {
        private Guid _projectID;
        private string _projectName;

        public Project()
        {
        }

        public Project(Guid projectID, string projectName)
        {
            _projectID = projectID;
            _projectName = projectName;
        }

        [Browsable(false)]
        public Guid ProjectID
        {
            get { return _projectID; }
            set { _projectID = value; }
        }

        [DisplayName("Name")]
        [Browsable(true)]
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }

        public override string ToString()
        {
            return ProjectName.ToString();
        }

        public string tableName()
        {
            return "project";
        }

        public Guid key()
        {
            return _projectID;
        }

        public string paramsForInsert()
        {
            return "(projectID, projectName)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + _projectName + "')";
        }

        public string valueForUpdate()
        {
            throw new NotImplementedException();
        }

        public string valueForQuery()
        {
            return "projectID=" + _projectID + "";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public System.Collections.Generic.List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                Project p = new Project();
                p.ProjectID = (Guid)row[0];
                p.ProjectName = Convert.ToString(row[1]);
                list.Add(p);
            }

            return list;
        }
    }
}