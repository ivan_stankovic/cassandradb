﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataModel
{
    public class Task : DomainObject
    {
        private Guid _taskID;
        private string _taskName;
        private string _taskDate;
        private string _plannedEndDate;
        private string _actualEndDate;
        private string _project;
        private string _typesOfTask;
        private string _participants;
        private Dictionary<string, string> _workersOnTask;

        [Browsable(false)]
        public Dictionary<string, string> WorkersOnTask
        {
            get { return _workersOnTask; }
            set { _workersOnTask = value; }
        }

        public Task()
        {
        }

        public Task(Guid taskID, string taskName, string taskDate, string plannedEndDate, string actualEndDate, string project, string typeOfTasks, string participants)
        {
            _taskID = taskID;
            _taskName = taskName;
            _taskDate = taskDate;
            _plannedEndDate = plannedEndDate;
            _actualEndDate = actualEndDate;
            _project = project;
            _typesOfTask = typeOfTasks;
            _participants = participants;
        }

        [Browsable(false)]
        public Guid TaskID
        {
            get { return _taskID; }
            set { _taskID = value; }
        }

        [DisplayName("Task name")]
        public string TaskName
        {
            get { return _taskName; }
            set { _taskName = value; }
        }

        [DisplayName("Task belong to project")]
        public string Project
        {
            get { return _project; }
            set { _project = value; }
        }

        [DisplayName("Type of task")]
        public string TypesOfTask
        {
            get { return _typesOfTask; }
            set { _typesOfTask = value; }
        }

        [DisplayName("Start task date")]
        public string TaskDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_taskDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _taskDate = value; }
        }

        [DisplayName("Planned end date of task")]
        public string PlannedEndDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_plannedEndDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _plannedEndDate = value; }
        }

        [DisplayName("Actual end date of task")]
        public string ActualEndDate
        {
            get
            {
                DateTime enteredDate = DateTime.Parse(_actualEndDate);
                return enteredDate.ToString("MM/dd/yyyy");
            }
            set { _actualEndDate = value; }
        }

        [DisplayName("Participant")]
        public string Participants
        {
            get { return _participants; }
            set { _participants = value; }
        }

        public string tableName()
        {
            return "task";
        }

        public Guid key()
        {
            return _taskID;
        }

        public string paramsForInsert()
        {
            return "(taskID, taskName, taskDate, plannedEndDate,	actualEndDate,	project, participants, typeOfTask)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + _taskName + "', '" + _taskDate + "', '" + _plannedEndDate + "', '" + _actualEndDate + "', '" + _project + "', " + _participants + ", '" + _typesOfTask + "')";
        }

        public string valueForUpdate()
        {
            return "taskName='" + _taskName + "', taskDate='" + _taskDate + "',  plannedEndDate='" + _plannedEndDate + "', actualEndDate='" + _actualEndDate + "', project='" + _project + "', participants=" + _participants + ", typeOfTask='" + _typesOfTask + "'";
        }

        public string valueForQuery()
        {
            return "taskID=" + _taskID + "";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();

            RowSet result = rows;
            foreach (Row row in result)
            {
                Task t = new Task();
                t._taskID = (Guid)row[0];
                t._actualEndDate = Convert.ToString(row[1]);
                IDictionary<string, string> tmp = (IDictionary<string, string>)row[2];
                Dictionary<string, string> workers = new Dictionary<string, string>();
                StringBuilder sb = new StringBuilder();
                if (tmp != null)
                {
                    if (tmp.Count != 0)
                    {
                        foreach (var item in tmp)
                        {
                            workers.Add(item.Key, item.Value);
                            sb.Append(item.Value);
                            sb.Append(", ");
                        }
                    }
                    else
                    {
                        t._participants = "";
                    }
                }
                else
                {
                    t._participants = "";
                }
                t._participants = sb.ToString();
                t._workersOnTask = workers;
                t._plannedEndDate = Convert.ToString(row[3]);
                t._project = Convert.ToString(row[4]);
                t._taskDate = Convert.ToString(row[5]);
                t._taskName = Convert.ToString(row[6]);
                t._typesOfTask = Convert.ToString(row[7]);

                list.Add(t);
            }

            return list;
        }
    }
}