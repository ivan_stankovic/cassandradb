﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DataModel
{
    public class TypeOfTasks : DomainObject
    {
        private Guid _typeID;
        private string _typeName;

        public TypeOfTasks()
        {
        }

        public TypeOfTasks(Guid typeID, string typeName)
        {
            _typeID = typeID;
            _typeName = typeName;
        }

        [Browsable(false)]
        public Guid TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }

        [DisplayName("Type of task")]
        [Browsable(true)]
        public string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }

        public override string ToString()
        {
            return TypeName.ToString();
        }

        public string tableName()
        {
            return "typesoftask";
        }

        public Guid key()
        {
            return _typeID;
        }

        public string paramsForInsert()
        {
            return "(typeID, typeName)";
        }

        public string valueForInsert()
        {
            return "(uuid(), '" + _typeName + "')";
        }

        public string valueForUpdate()
        {
            throw new NotImplementedException();
        }

        public string valueForQuery()
        {
            return "typeID=" + _typeID + "";
        }

        public string valueForSelect(DomainObject domain)
        {
            return "";
        }

        public System.Collections.Generic.List<DomainObject> Fill(Cassandra.RowSet rows)
        {
            List<DomainObject> list = new List<DomainObject>();
            RowSet result = rows;
            foreach (Row row in result)
            {
                TypeOfTasks tot = new TypeOfTasks();
                tot.TypeID = (Guid)row[0];
                tot.TypeName = Convert.ToString(row[1]);
                list.Add(tot);
            }

            return list;
        }
    }
}