-- Create KEYSPACE
CREATE KEYSPACE ncmaster
WITH REPLICATION =   { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

-- Enter in KEYSPACE
USE ncmaster;

-- Tables and queries
CREATE TABLE ncmaster.paymentbasis (
	basisid uuid PRIMARY KEY,
	basis double); 

INSERT INTO ncmaster.paymentbasis (basisid, basis) VALUES (uuid(), 2000.00);

2014-09-08 20:57:08
INSERT INTO coworker (coWorkerID, personID, firstName, lastName, dateFrom, dateTo, compensation) VALUES (uuid(), '534', 'sdf', 'vcv', '2014-09-08 20:57:08', '2014-12-08 20:57:08', 332)

CREATE TABLE ncmaster.project (
	projectID uuid PRIMARY KEY,
	projectName text);

CREATE TABLE ncmaster.typesoftask (
	typeID uuid PRIMARY KEY,
	typeName text);

CREATE TABLE ncmaster.employee (
	employeeID uuid,
	personID text, 
	firstName text, 
	lastName text, 
	workInDepartmant text,
	leaderOfDepartmant boolean,
	PRIMARY KEY(employeeID, personID));

CREATE TABLE ncmaster.coworker (
	coWorkerID uuid,
	personID text, 
	firstName text, 
	lastName text, 
	dateFrom timestamp,
	dateTo timestamp,
	compensation double,
	PRIMARY KEY(coWorkerID, personID));

CREATE TABLE ncmaster.payment (
	paymentID uuid, 
	paymentDate timestamp,
	personID text,
	person text, 
	payment double, 
	paymentbasis double,
	PRIMARY KEY(paymentID, personID));

CREATE TABLE ncmaster.organizationunit (
	organizationUnitID uuid PRIMARY KEY, 
	organizationUnitName text,
	personID text,
	leaderOfDepartmant text,
	place text,
	team list<text>); --organizationUnitName

CREATE TABLE ncmaster.task (
	taskID uuid PRIMARY KEY,
	taskName text,
	taskDate timestamp,
	organizationUnitName text,
	plannedEndDate timestamp,
	actualEndDate timestamp,
	project text,
	participants map<text, text>, --personID, person
	typeOfTask text);